<!DOCTYPE html>
<html lang="en">

<head>
    <script src="{{url('js/pusher.min.js')}}"></script>

    <style>


        body {
            font-size:14px;
        }
        .activeDroppable {
            background-color: #eeffee;
        }
        .hoverDroppable {
            background-color: lightgreen;
        }
        .draggableField {
            /* float: left; */

        }
        .selectorField .well.well-mini {
            margin-bottom: 0px;
            padding: 3px;
        }
        .draggableField > input,select, button, .checkboxgroup, .selectmultiple, .radiogroup {
            margin-top: 10px;
            margin-right: 10px;
            margin-bottom: 10px;
        }
        /*.draggableField:hover{
          background-color: #ccffcc;
        }*/
        .control-label {
            width: 100px;
        }
        .selectorField .textbox b, .selectorField .password b, .selectorField .combobox b, .selectorField .radiogroup b, .selectorField .checkboxgroup b, .selectorField .selectmultiple b,
        .selectorField .displaydate b, .selectorField .displaytext b {
            position:relative; top:3px; left: 0px; width: 18px; height: 16px; display: inline-block; background-image: url("{{url('img/sprite.png')}}"); background-repeat: no-repeat;
        }
        .selectorField .textbox b, .selectorField .password b, .selectorField .displaytext b {
            background-position: -10px -549px;
        }
        .selectorField .combobox b {
            background-position: -10px -722px;
        }
        .selectorField .radiogroup b {
            background-position: -10px -619px;
        }
        .selectorField .checkboxgroup b {
            background-position: -10px -688px;
        }
        .selectorField .selectmultiple b {
            background-position: -10px -1082px;
        }
        .selectorField .displaydate b {
            background-position: -10px -975px;
        }

        .well-mini {
            margin: 4px;
            padding: 2px;
            border-radius: 6px;
            width: 160px;
            cursor: pointer;
        }
        .draggableField:hover .well-mini {
            background-color: #ccffcc;
        }
        .modele {
            display:none;
        }
        .navbar {
            border: 1px solid transparent;
            margin-bottom: 20px;
            min-height: 70px !important;
            position: relative;
        }
        .sidebar {
            margin-top: 71px !important;
            position: absolute;
            width: 250px;
            z-index: 1;
        }
        .navbar-brand {
            float: left;
            font-size: 18px;
            height: 70px !important;
            line-height: 20px;
            padding: 15px;
        }
    </style>
    <style id="content-styles">
        /* Styles that are also copied for Preview */
        body {
            margin: 10px 0 0 10px;
        }
        .control-label {
            display: inline-block !important;
            pasdding-top: 5px;
            text-align: right;
            vertical-align: baseline;
            padding-right: 10px;
        }
        .droppedField {
            padding-left:5px;
        }
        .droppedField > input,select, button, .checkboxgroup, .selectmultiple, .radiogroup {
            margin-top: 10px;
            margin-right: 10px;
            margin-bottom: 10px;
        }
        .action-bar .droppedField {
            float: left;
            padding-left:5px;
        }
    </style>

    <Style>
        input[type="date"] {
            width: 130px;
        }
        #side-menu{
            font-family: Lato;
            font-size: 14px;
            font-style: normal;
            font-variant: normal;
            font-weight: 500;
            line-height: 26px;

        }

        .navbar-header   {
            background-color:#CAD7EB;

        }

        .navbar-brand > img {
            max-height: 100%;
            height: 100%;
            -o-object-fit: contain;
            object-fit: contain;
        }

    </Style>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ideagen Reporting</title>

    <!-- Lato Font -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato" />
    <!-- Bootstrap Core CSS -->
    <link href="{{ url('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ url('css/metisMenu.css')}}" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="{{ url('css/ideagenreporting.css')}}" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="{{ url('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ url('css/jquery-ui.css')}}" rel="stylesheet" type="text/css">
    <!-- jQuery -->
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/jqueryui/ui/minified/jquery-ui.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/handlebars.js')}}"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{url('js/metisMenu.min.js')}}"></script>
    <script src="{{url('js/handlebars.js')}}"></script>
    <script src="{{url('js/f_builder.js')}}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{url('js/ideagenreporting.js')}}"></script>
    <style>
        .hides {
            display: none;
        }
        .modal {
            background-clip: padding-box;
            background-color: #fff;
            border: 1px solid rgba(0, 0, 0, 0.3);
            border-radius: 6px;
            box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
            left: 50%;
            margin-left: -280px;
            outline: 0 none;
            position: fixed;
            top: 10%;
            width: 560px;
            z-index: 1050;
            bottom: auto;
        }
        .modal.fade.in {
            top: 10%;
        }
        .modal.fade {
            top: -25%;
            transition: opacity 0.3s linear 0s, top 0.3s ease-out 0s;
        }
        .activeDroppable {
            background-color: #eeffee;
        }

        .hoverDroppable {
            background-color: lightgreen;
        }

        .draggableField {
            /* float: left; */
            padding-left:5px;
        }

        .draggableField > input,select, button, .checkboxgroup, .selectmultiple, .radiogroup {
            margin-top: 10px;

            margin-right: 10px;
            margin-bottom: 10px;
        }

        .draggableField:hover{
            background-color: #ccffcc;
        }
        .draggableField > input, select, button, .checkboxgroup, .selectmultiple, .radiogroup {
            margin-bottom: 10px;
            margin-right: 10px;
            margin-top: 0px !important;
        }
    </style>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: "POST",
            data: {'notification':'gib'},
            url: "{{ url('/notification/index') }}"
        }).done(function( msg ) {
            console.log(msg);
        });


        var pusher = new Pusher("6a6d0e72e54264334404", {
            cluster: 'eu'
        });
        var counter;
        var vars = {};
        $.ajax({
            method: "POST",
            data: {'notification':'gib'},
            url: "{{ url('/users/groups/get') }}"
        }).done(function( msg ) {
            $.each( msg, function( key, value ) {
                var val = String(value['id']);
                vars ['channel' + counter] = pusher.subscribe(val);

                vars ['channel' + counter].bind('note', function(data) {
                    $('#notifications').css('color', 'red');
                    $('#notifications_container').prepend('<li><a href="#"><div><strong>'+ data[0] +'</strong><span class="pull-right text-muted"><em>' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds() + '</em></span></div><div>'+ data[1] +'</div></a></li><li class="divider"></li>');
                    $.ajax({
                        type: "POST",
                        data: {'notification':data},
                        url: "{{ url('/notification/save') }}"
                    });
                });

                counter++;
            });
        });

        var channels = pusher.subscribe('all');
        var dt = new Date();

        channels.bind('note', function(data) {
            $('#notifications').css('color', 'red');
            $('#notifications_container').append('<li><a href="#"><div><i class="fa fa-comment fa-fw">' + data[0] + '</i><span class="pull-right text-muted small">' + data[1] + '</span>/div></a></li>');
            $.ajax({
                type: "POST",
                data: {'notification':data},
                url: "{{ url('/notification/save') }}"
            });
        });
        // Added Pusher logging
        Pusher.log = function(msg) {
            console.log(msg);
        };

        $.ajax({
            method: "GET",
            url: "{{ url('http://ip3.kmilew22.ayz.pl//msgs/index') }}"
        });
    </script>
    </head>



<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ url('/') }}" class="navbar-brand logo"><img src="{{ url('img/logo-workstream.png') }}"/></a>

        </div>
        <!-- /.navbar-header -->
        @if(Auth::check())
            <ul class="nav navbar-top-links navbar-right">
               Currently logged as {!! $role !!}

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i id="notifications" class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul id="notifications_container" class="dropdown-menu dropdown-alerts">
                        <! -- ALERTS -->
                        @if ($notifications != null)
                            @foreach($notifications as $notification)
                                <li>
                                    <a href="#">
                                        <div>
                                            <strong>{!! $notification['name'] !!}</strong>
                                    <span class="pull-right text-muted">
                                        <em>{!! $notification['created_at'] !!} </em>
                                    </span>
                                        </div>
                                        <div>{!! $notification['content'] !!}</div>
                                    </a>
                                </li>
                                <li class="divider"></li>
                            @endforeach
                        @else
                            <p>No notifications</p>
                        @endif
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Notifications</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ url('users/edit?id=' . Auth::user()->id) }}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
        @endif


        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="{{ url('/') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    @if($role == 'admin' || $role == 'manager')
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Notifications<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{url('notification/write')}}">Send new group notification</a>
                                </li>
                                <li>
                                    <a href="{{url('notification/showAll')}}">Show All</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    @endif
                    @if($role == 'admin')
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Users<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{url('users')}}">Existing</a>
                                </li>
                                <li>
                                    <a href="{{url('users/create')}}">Create New</a>
                                </li>
                                <li>
                                    <a href="{{url('users/groups')}}">Groups</a>
                                </li>
                                <li>
                                    <a href="{{url('users/groups/create')}}">Create Group</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    @endif
                    @if($role == 'admin' || $role == 'manager')
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Reports Templates<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{url('forms')}}">View All</a>
                                </li>
                                <li>
                                    <a href="{{url('forms/create')}}">Create</a>
                                    <!-- /.nav-third-level -->
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    @endif

                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i> Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{url('forms/submit/submitted')}}">View All Submitted</a>
                            </li>
                            <li>
                                <a href="{{url('forms/submit/archived')}}">View All Archived</a>
                            </li>
                            <li>
                                <a href="{{url('forms/submit')}}">Submit New</a>
                                <!-- /.nav-third-level -->
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>




                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>



</div>
<!-- /#wrapper -->
<div id="page-wrapper">
@yield('content')
        </div>
<span id="footer">
   @yield('footer')
</span>

</body>

</html>
