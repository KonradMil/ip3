@extends('layouts.app')

@section('content')

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Existing Users
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>E-mail</th>
                                        <th>Registered At</th>
                                        <th>CRUD</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if ($users != null)
                                        @foreach($users as $user)
                                            <tr class="gradeA">
                                                <td>{!! $user['id'] !!}</td>
                                                <td>{!! $user['name'] !!}</td>
                                                <td>{!! $user['email'] !!}</td>
                                                <td class="center">{!! $user['created_at'] !!}</td>
                                                <td>
                                                <a href="{{url('users/delete?id=' . $user['id'])}}">DELETE</a>
                                                <a href="{{url('users/edit?id=' . $user['id'])}}">EDIT</a></td>
                                            </tr>


                                        @endforeach
                                    @else
                                        <p> Nufing</p>
                                    @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Success!</strong> {{ Session::get('message', '') }}
                            </div>
                        @endif
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            @stop
@section('footer')

                    <!-- DataTables JavaScript -->
            <script src="js/datatables/media/js/jquery.dataTables.min.js"></script>
            <script src="js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

            <script>
                $(document).ready(function() {
                    $('#dataTables-example').DataTable({
                        responsive: true
                    });
                });
            </script>
@endsection
