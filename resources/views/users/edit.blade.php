@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Edit</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/users/edited') }}">
                            {!! csrf_field() !!}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Name</label>

                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" value="{{ $user['name'] }}">

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{ $user['id'] }}">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ $user['email'] }}">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Phone Number</label>

                                <div class="col-md-6">
                                    <input type="tel" class="form-control" name="phone" value="{{ $user['phone_nr'] }}">

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password" value="">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has($user['roles']) ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Role</label>


                                <div class="col-md-6">
                                    <div class="radio">
                                        @if($user['roles']== 'admin')

                                        <label><input type="radio" name="role" checked="checked" value="admin">Admin</label>
                                            @else
                                            <label><input type="radio" name="role" value="admin">Admin</label>
                                            @endif
                                    </div>
                                    <div class="radio">
                                        @if($user['roles'] == 'user')
                                            <label><input type="radio" name="role" checked="checked" value="user">User</label>
                                        @else
                                            <label><input type="radio" name="role" value="user">User</label>
                                        @endif

                                    </div>
                                    <div class="radio">
                                        @if($user['roles'] == 'manager')
                                            <label><input type="radio" name="role" checked="checked" value="manager">Manager</label>
                                        @else
                                            <label><input type="radio" name="role" value="manager">Manager</label>
                                        @endif

                                    </div>

                                    @if ($errors->has($user['roles']))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('roles') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('groups') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">Group</label>


                                <div class="col-md-6">
                                    <?php $ar = []; ?>
                                        @foreach ($groups as $group)
                                        @foreach ($user_groups as $user_group)
                                            @if($group['id'] == $user_group['id'] )
                                                <?php array_push($ar,$user_group['id']); ?>
                                            @endif
                                            @endforeach
                                        @endforeach
                                        @foreach ($groups as $group)
                                            @if(in_array($group['id'],$ar))
                                        <input type="checkbox" value="{{ $group['id'] }}" name="group[]" checked>{{ $group['name'] }}</input>
                                                @else
                                                <input type="checkbox" value="{{ $group['id'] }}" name="group[]">{{ $group['name'] }}</input>
                                            @endif
                                        @endforeach


                                @if ($errors->has('group'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('group') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i>Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection