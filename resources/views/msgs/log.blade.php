@extends('layouts.app')

@section('content')
    <div class="col-md-6">
        @foreach($sms_sent as $forma)
            <h4>System</h4>
           <p>
               {!! $forma{'content'} !!}
           </p>
            <p><i>{!! $forma['created_at'] !!}</i></p>
        @endforeach
    </div>
    <div class="col-md-6">
        @foreach($sms_rec as $form)
            <h4>User</h4>
            <p>
                {!! $form{'content'} !!}
            </p>
            <p><i>{!! $form['created_at'] !!}</i></p>
        @endforeach
    </div>

@endsection
