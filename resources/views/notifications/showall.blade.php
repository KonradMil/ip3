@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Existing notifications
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Content</th>
                                <th>Created At</th>
                                <th>CRUD</th>
                            </tr>
                            </thead>
                            <tbody>


                                @foreach($user as $use)

                                    <tr class="gradeA">
                                        <td>{{ $use['id'] }}</td>
                                        <td>{{ $use['name'] }}</td>
                                        <td class="center">{!! $use['content'] !!}</td>
                                        <td class="center">{{ $use['created_at'] }}</td>
                                        <td>
                                            <a href="{{url('notification/delete?id=' . $use['id'])}}">DELETE</a>
                                        </td>
                                    </tr>
                                @endforeach


                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Success!</strong> {{ Session::get('message', '') }}
                </div>
            @endif
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    @stop
    @section('footer')

            <!-- DataTables JavaScript -->
    <script src="js/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@endsection
