<!--
  Starting templates declaration
  DEV-NOTE: Keeping the templates and code simple here for demo  -- use some better template inheritance for multiple controls
--->

<script id="control-customize-template" type="text/x-handlebars-template">
    <div class="modal-header">
        <h3>{{header}}</h3>
    </div>
    <div class="modal-body">
        <form id="theForm" class="form-horizontal">
            <input type="hidden" value="{{type}}" name="type"/>
            <input type="hidden" value="{{forCtrl}}" name="forCtrl"/>
            <p id="pLibelle"><label class="control-label" for="handlebars-textbox-label">Label</label> <input type="text" name="label" value="" id="handlebars-textbox-label"/></p>
            <p style="display:{{displayNom}}"><label class="control-label" for="handlebars-textbox-name">Name</label> <input type="text" value="" name="name" id="handlebars-textbox-name"/></p>
            <p id="pObligatoire"><label class="control-label" for="checkbox-obligatoire-textbox">Mandatory</label> <input type="checkbox" name="obligatoire" value="" id="checkbox-obligatoire-textbox"/></p>
            {{{content}}}
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" onclick='save_customize_changes()'>Save</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick='delete_ctrl()'>Remove</button>
    </div>
</script>

<script id="textbox-template" type="text/x-handlebars-template">
    <p><label class="control-label">Placeholder</label> <input type="text" name="placeholder" value=""/></p>
</script>

<script id="combobox-template" type="text/x-handlebars-template">
    <p><label class="control-label">Options</label> <textarea name="options" rows="5"></textarea></p>
</script>

<script id="text-template" type="text/x-handlebars-template">
    <p><label class="control-label" for="handlebars-textbox-text">Text</label> <textarea name="texte" rows="10" id="handlebars-textbox-text"></textarea></p>
</script>

<script id="date-template" type="text/x-handlebars-template">
    <p>
        <label class="control-label" for="handlebars-textbox-formatdate" style="padding-top:16px;">Format</label>
        <select name="dateformat" id="handlebars-textbox-formatdate">
            <option value="DD/MM/YYYY">DD/MM/YYYY</option>
            <option value="DD/MM/YYYY hh:mm:ss">DD/MM/YYYY hh:mm:ss</option>
        </select>
    </p>
</script>

<!-- End of templates -->