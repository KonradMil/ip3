

@section('content')
    <div class="row-fluid">

@if(Session::has('success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Success!</strong> {{ Session::get('message', '') }}
    </div>
    @endif
    </div>


    @endsection