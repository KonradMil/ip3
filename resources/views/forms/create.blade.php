@extends('layouts.app')


@section('content')

    <div class="row-fluid">
        <div class="col-md-2" style="">

            <b>Input fields</b><br/>
            <div class='selectorField draggableField'>
                <div class="textbox well well-mini"><b></b> Simple Text</div>
                <div class='modele'>
                    <label class="control-label">Text</label>
                    <input type="text" placeholder="Your text.." class="ctrl-textbox"/>
                    <input type="hidden" class="hiddenObligatoire"/>
                </div>
            </div>

            <div class='selectorField draggableField'>
                <div class="password well well-mini"><b></b>Password</div>
                <div class='modele'>
                    <label class="control-label">Password</label>
                    <input type="password" placeholder="Password ..." class="ctrl-passwordbox"/>
                    <input type="hidden" class="hiddenObligatoire"/>
                </div>
            </div>

            <div class='selectorField draggableField'>
                <div class="combobox well well-mini"><b></b> List</div>
                <div class='modele'>
                    <label class="control-label">List</label>
                    <select class="ctrl-combobox">
                        <option name="list[]" value="option1">Option 1</option>
                        <option value="list[]">Option 2</option>
                        <option value="list[]">Option 3</option>
                    </select>
                    <input type="hidden" class="hiddenObligatoire"/>
                </div>
            </div>

            <div class='selectorField draggableField'>
                <div class="radiogroup well well-mini"><b></b> Radio list</div>
                <div class='modele'>
                    <label class="control-label" style="vertical-align:top">Radio list</label>
                    <div style="display:inline-block;" class="ctrl-radiogroup">
                        <span style="display:block;"><input type="radio" name="radioField[]"
                                                            value="option1"/>Option 1</span>
                        <span style="display:block;"><input type="radio" name="radioField[]"
                                                            value="option2"/>Option 2</span>
                        <span style="display:block;"><input type="radio" name="radioField[]"
                                                            value="option3"/>Option 3</span>
                    </div>
                    <input type="hidden" class="hiddenObligatoire"/>
                </div>
            </div>

            <div class='selectorField draggableField'>
                <div class="checkboxgroup well well-mini"><b></b>Multiple choice chceckbox</div>
                <div class='modele'>
                    <label class="control-label" style="vertical-align:top">Multiple choice chceckbox</label>
                    <div style="display:inline-block;" class="ctrl-checkboxgroup">
                        <span style="display:block;"><input type="checkbox" name="checkboxField[]" value="option1"/>Option 1</span>
                        <span style="display:block;"><input type="checkbox" name="checkboxField[]" value="option2"/>Option 2</span>
                        <span style="display:block;"><input type="checkbox" name="checkboxField[]" value="option3"/>Option 3</span>
                    </div>
                    <input type="hidden" class="hiddenObligatoire"/>
                </div>
            </div>

            <div class='selectorField draggableField'>
                <div class="selectmultiple well well-mini"><b></b>Multiple Selection</div>
                <div class='modele'>
                    <label class="control-label" style="vertical-align:top">Multiple Selection</label>
                    <div style="display:inline-block;">
                        <select multiple="multiple" style="width:150px" class="ctrl-selectmultiplelist">
                            <option value="option1" name="multi[]">Option 1</option>
                            <option value="option2" name="multi[]">Option 2</option>
                            <option value="option3" name="multi[]">Option 3</option>
                        </select>
                    </div>
                    <input type="hidden" class="hiddenObligatoire"/>
                </div>
            </div>


            <b>Auto Fields</b><br/>
            <div class='selectorField draggableField'>
                <div class="displaydate well well-mini"><b></b> Date</div>
                <div class='modele'>
                    <label class="control-label">Date</label>
                    <input type="date" name="date" value="<?php echo date('m.d.y H:m:s'); ?>">
                </div>
            </div>

            <div class='selectorField draggableField'>
                <div class="displaytext well well-mini"><b></b> Text</div>
                <div class='modele'>
                    <label class="control-label">Text</label>
                    <span class="ctrl-text">Text</span>
                </div>
            </div>


            <b>Buttons</b><br/>

            <div class='selectorField draggableField'>
                <button class="btn btn-success ctrl-btn"><i class="icon-ok-sign icon-white"></i> Save Button</button>
            </div>

            <!-- End of list of controls -->
        </div>


        <div class="col-md-10">
            <div class="row-fluid" id="form-title-div">
                <form role="form">
                    <div class="form-group  col-sm-6">
                        <input class="form-control input-lg " placeholder="Your title" id="form-title" tabindex="1"
                               name="form-title" data-bind="value: form-title" type="text" maxlength="70"/>
                    </div>
                  
                </form>
            </div>
            <!--
              Below we have the columns to drop controls
                -- Removed the TABLE based implementations from earlier code
                -- Grid system used for rendering columns
                -- Columns can be simply added by defining a div with droppedFields class
            -->
            <div class="col-md-12" id="selected-content">


                <div class="row-fluid">
                    <div class="col-md-6 well droppedFields"></div>
                    <div class="col-md-6 well droppedFields"></div>
                </div>
                <!-- Action bar - Suited for buttons on form -->
                <div class="row-fluid">
                    <div class="col-md-12 well action-bar droppedFields" style="min-height:80px;"></div>
                </div>
            </div>


        </div>
    </div>



    <!-- Preview button -->
    <div class="row-fluid">
        <div class="col-md-12">
            <input type="button" class="btn btn-primary" value="Save" onclick="save();"/>
            <input type="button" class="btn btn-primary" value="Preview" onclick="preview();"/>
            <input type="button" class="btn btn-primary" value="Add row"
                   onclick="$('#dialog-form-nombre-colonne').modal('show'); $('#dialog-form-nombre-colonne').css('z-index', '1500');"/>
        </div>
    </div>


    <div class="tabbable">
        <!-- List of controls rendered into Bootstrap Tabs -->


    </div>
    <div class="answer"></div>

    <!--
      Starting templates declaration
      DEV-NOTE: Keeping the templates and code simple here for demo  -- use some better template inheritance for multiple controls
    --->

    <script id="control-customize-template" type="text/x-handlebars-template">
        <div class="modal-header">
            <h3>@{{header}}</h3>
        </div>
        <div class="modal-body">
            <form id="theForm" class="form-horizontal">
                <input type="hidden" value="@{{type}}" name="type"/>
                <input type="hidden" value="@{{forCtrl}}" name="forCtrl"/>
                <p id="pLibelle"><label class="control-label" for="handlebars-textbox-label">Label</label>
                    <input type="text" name="label" value="" id="handlebars-textbox-label"/></p>
                <p style="display:@{{displayNom}}"><label class="control-label"
                                                          for="handlebars-textbox-name">Name</label> <input type="text"
                                                                                                            value=""
                                                                                                            name="name"
                                                                                                            id="handlebars-textbox-name"/>
                </p>
                <p id="pObligatoire"><label class="control-label" for="checkbox-obligatoire-textbox">Required</label>
                    <input type="checkbox" name="obligatoire" value="" id="checkbox-obligatoire-textbox"/></p>
                @{{{content}}}
            </form>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" onclick='save_customize_changes()'>Save</button>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" onclick='delete_ctrl()'>Delete
            </button>
        </div>
    </script>

    <script id="textbox-template" type="text/x-handlebars-template">
        <p><label class="control-label">Placeholder</label> <input type="text" name="placeholder" value=""/></p>
    </script>

    <script id="combobox-template" type="text/x-handlebars-template">
        <p><label class="control-label">Options</label> <textarea name="options" rows="5"></textarea></p>
    </script>

    <script id="text-template" type="text/x-handlebars-template">
        <p><label class="control-label" for="handlebars-textbox-text">Text</label> <textarea name="texte" rows="10"
                                                                                             id="handlebars-textbox-text"></textarea>
        </p>
    </script>

    <script id="date-template" type="text/x-handlebars-template">
        <p>
            <label class="control-label" for="handlebars-textbox-formatdate" style="padding-top:16px;">Format</label>
            <select name="dateformat" id="handlebars-textbox-formatdate">
                <option value="DD/MM/YYYY">DD/MM/YYYY</option>
                <option value="DD/MM/YYYY hh:mm:ss">DD/MM/YYYY hh:mm:ss</option>
            </select>
        </p>
    </script>

    <!-- End of templates -->

    <div id="dialog-form-nombre-colonne" class="modal hides fade" style="display: none; ">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">x</a>
            <h3>Create Table</h3>
        </div>
        <div class="modal-body" style="padding:10px 40px 0 40px; min-height:150px;">
            <form>
                <label for="nbColonne">Number of columns<span id="nbColonne"> 1</span></label>
                <div id="sliderNbColonne"></div>
            </form>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn btn-success" onclick='ajouterTableau();'>Add Table</a>
            <a href="#" class="btn" data-dismiss="modal">Close</a>
        </div>
    </div>

    <span style="position:absolute; padding-right:6px; height:10px;" id="divDeleteTableau">
  <a href="#" onclick="supprimerTableau()"><img src="{{url('img/delete.png')}}" alt=""/></a>
</span>

    <script>
        $(document).ready(docReady);
        function save() {
            console.log('Save clicked');
            var res = [];
            // Sample preview - opens in a new window by copying content -- use something better in production code
            var selected_content = $("#selected-content").clone();
            selected_content.find("div").each(function (i, o) {
                var obj = $(o)
                obj.removeClass("draggableField ui-draggable well ui-droppable ui-sortable");
            });
            var legend_text = $("#form-title")[0].value;
            res[0] = legend_text;
            if (legend_text == "") {
                legend_text = "Form";
            }
            selected_content.find("#form-title-div").remove();

            var selected_content_html = selected_content.html();

            var dialogContent = '';

            dialogContent += selected_content_html;
            res[1] = dialogContent;
            $.ajax({
                type: "POST",
                data: {res: res},
                url: "{{ url('/forms/saved') }}",
                success: function (msg) {
                    $('.answer').html(msg);
                }
            });

            console.log(dialogContent);
            //var win = window.open("about:blank");
            //win.document.write(dialogContent);
        }
    </script>






@endsection