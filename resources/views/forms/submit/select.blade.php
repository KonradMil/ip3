@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Your submitted reports
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Submitted On</th>
                                <th>Edited On</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($forms != null)
                                @foreach($forms as $form)
                                    @if($form['archived'] != 1)
                                        @if($form['highlight'])
                                            <tr class="gradeA" style="background-color: rgba(255, 0, 0, 0.3);">
                                        @else
                                            <tr class="gradeA">
                                                @endif

                                                <td class="center">{!! $form['id'] !!}</td>
                                                <td>{!! $form['name'] !!}</td>
                                                <td class="center">{!! $form['created_at'] !!}</td>
                                                <td class="center">{!! $form['updated_at'] !!}</td>
                                                <td class="center"><a
                                                            href="{{url('forms/submit/display?id=' .$form['id'] )}}">Display</a>
                                                    || <a href="{{url('forms/submit/archive?id=' .$form['id'] )}}">Archive</a>
                                                    ||
                                                    <a href="{{url('msgs/send/send?id=' .$form['id'] . '&us_id=' . $form['user_id'] )}}">Send
                                                        SMS</a> || <a
                                                            href="{{url('msgs/shows?id=' .$form['id'] . '&title=' . $form['name'])}}">SMS
                                                        Log</a></td>
                                            </tr>


                                        @endif
                                        @endforeach
                                    @else
                                        <td class="center" colspan="5">No forms to show</td>
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Success!</strong> {{ Session::get('message', '') }}
                </div>
            @endif
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    @stop
    @section('footer')

            <!-- DataTables JavaScript -->
    <script src="{{url('js/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@endsection

