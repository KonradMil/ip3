@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Your submitted reports
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Submitted On</th>
                                <th>Edited On</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($forms != null)
                                @foreach($forms as $form)
                                        <tr class="gradeA">
                                            <td class="center">{!! $form['id'] !!}</td>
                                            <td>{!! $form['name'] !!}</td>
                                            <td class="center">{!! $form['created_at'] !!}</td>
                                            <td class="center">{!! $form['updated_at'] !!}</td>
                                            <td class="center"><a href="{{url('forms/submit/display?id=' .$form['id'] )}}">Display</a> <a href="{{url('forms/submit/unarchive?id=' .$form['id'] )}}">Pull out from archives</a></td>
                                        </tr>
                                @endforeach
                            @else
                                <p> Nufing</p>
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    @stop
    @section('footer')

            <!-- DataTables JavaScript -->
    <script src="js/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="js/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });
    </script>
@endsection
