@extends('layouts.app')

@section('content')
    @foreach($form as $forma)

        <h3>{{$forma['name']}}</h3>
        <form action="{{url('forms/submit/submitnew')}}" method="post">
            <input name="id" hidden value="{{ $forma['id'] }}">
            {!! $forma['content'] !!}



            <input type="submit" value="Submit">

        </form>

  @endforeach
@endsection

  
@section('footer')
<script>
    {!! $hiddenDivBeginning !!}
</script>
    @endsection