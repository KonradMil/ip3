@extends('layouts.app')

@section('content')
    <h3>{{$form['name']}}</h3>
   <div class="form_wrapper">
       <form action="{{ url('forms/submit/submitnew') }}" method="post">
       {!! $form['content'] !!}

           <br/> <br/> <br/><input type="submit" value="Submit">
       </form>

   </div>
@stop