/**
 * Created by Konrad on 11.03.2016.
 */
var text;
var passwordbox;
var list;
var date;
var textbox;
var selectmultiplelist;
var checkboxgroup;

var classList = $('#divId').attr('class').split(/\s+/);
$.each(classList, function (index, item) {
    switch (item) {
        case 'ctrl-text':
            text+=1;
            break;
        case 'ctrl-date':
            date+=1;
            break;
        case 'ctrl-textbox':
            textbox+=1;
            break;
        case 'ctrl-passwordbox':
            passwordbox+=1;
            break;
        case 'ctrl-selectmultiplelist':
            selectmultiplelist+=1;
            break;
        case 'ctrl-checkboxgroup':
            checkboxgroup+=1;
            break;
        case 'ctrl-radiogroup':
            radiogroup+=1;
            break;
    }
});