<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Msgs extends Model
{
    protected $fillable = [
        'name', 'content', 'recipient_id', 'sender_id', 'form_id', 'read'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
