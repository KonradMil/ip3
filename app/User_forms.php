<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_forms extends Model
{
    protected $fillable = [
        'user_id', 'form_id', 'submittion_id', 'archived', 'highlight'
    ];
}
