<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {


    Route::get('/login', function () {
        return view('login');
    });

    Route::get('/', 'BaseController@index', ['middleware' => 'auth', function () {     }]);
    Route::get('notification/write', 'NotificationController@write', ['middleware' => 'auth', function () {     }]);
    Route::get('notification/showAll', 'NotificationController@showAll', ['middleware' => 'auth', function () {     }]);
    Route::get('notification/delete', 'NotificationController@delete', ['middleware' => 'auth', function () {     }]);
    Route::post('notification/sendtogroup', 'NotificationController@sendToGroup', ['middleware' => 'auth', function () {     }]);
    Route::post('notification/index', 'NotificationController@index', ['middleware' => 'auth', function () {     }]);

    Route::get('forms/submit/archived', 'SubmittionController@showArchived', ['middleware' => 'auth', function () {     }]);
    Route::get('forms/submit/unarchive', 'SubmittionController@unarchive', ['middleware' => 'auth', function () {     }]);
    Route::get('forms/submit/archive', 'SubmittionController@archive', ['middleware' => 'auth', function () {     }]);
    Route::get('forms', 'FormsController@index', ['middleware' => 'auth', function () {     }]);
    Route::get('forms/create', 'FormsController@create', ['middleware' => 'auth', function () {    }]);
    Route::get('forms/show', 'FormsController@show', ['middleware' => 'auth', function () {    }]);
    Route::get('forms/delete', 'FormsController@delete', ['middleware' => 'auth', function () {    }]);
    Route::post('forms/saved', 'FormsController@saved', ['middleware' => 'auth', function () {    }]);
    Route::get('forms/submit/', 'SubmittionController@index', ['middleware' => 'auth', function () {    }]);
    Route::get('forms/submit/fillin', 'SubmittionController@fillin', ['middleware' => 'auth', function () {    }]);
    Route::get('forms/submit', 'SubmittionController@index', ['middleware' => 'auth', function () {    }]);
    Route::get('forms/submit/submitted', 'SubmittionController@select', ['middleware' => 'auth', function () {    }]);
    Route::post('forms/submit/submitnew', 'SubmittionController@save', ['middleware' => 'auth', function () {    }]);
    Route::get('forms/submit/test', 'SubmittionController@test', ['middleware' => 'auth', function () {    }]);
    Route::get('forms/submit/display', 'SubmittionController@display', ['middleware' => 'auth', function () {    }]);

    Route::get('users', 'UsersController@index', ['middleware' => 'auth', function () {     }]);
    Route::get('users/create', 'UsersController@create', ['middleware' => 'auth', function () {    }]);
    Route::post('users/created', 'UsersController@created', ['middleware' => 'auth', function () {    }]);
    Route::get('users/show', 'UsersController@show', ['middleware' => 'auth', function () {    }]);
    Route::get('users/delete', 'UsersController@delete', ['middleware' => 'auth', function () {    }]);
    Route::get('users/edit', 'UsersController@edit', ['middleware' => 'auth', function () {    }]);
    Route::post('users/edited', 'UsersController@edited', ['middleware' => 'auth', function () {    }]);
    Route::post('users/groups/get', 'UsersController@getUserGroups', ['middleware' => 'auth', function () {    }]);


    Route::get('msgs/shows', 'SubmittionController@log', ['middleware' => 'auth', function () {     }]);
    Route::get('msgs/send/send', 'MsgController@write', ['middleware' => 'auth', function () {     }]);
    Route::get('msgs/index', 'MsgController@saveToDB', ['middleware' => 'auth', function () {     }]);
    Route::get('msgs', 'MsgController@index', ['middleware' => 'auth', function () {     }]);
    Route::get('msgs/write', 'MsgController@write', ['middleware' => 'auth', function () {     }]);
    Route::get('msgs/show', 'MsgController@show', ['middleware' => 'auth', function () {     }]);
    Route::get('msgs/showPerForm', 'MsgController@showFormMsgs', ['middleware' => 'auth', function () {     }]);
    Route::post('msgs/sendsms', 'MsgController@sendSms', ['middleware' => 'auth', function () {     }]);

    Route::get('users/groups', 'GroupController@index', ['middleware' => 'auth', function () {     }]);
    Route::get('users/groups/create', 'GroupController@create', ['middleware' => 'auth', function () {     }]);
    Route::get('users/groups/delete', 'GroupController@delete', ['middleware' => 'auth', function () {     }]);
    Route::post('users/groups/created', 'GroupController@created', ['middleware' => 'auth', function () {     }]);
    Route::controller('notifications', 'NotificationController');

});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});
