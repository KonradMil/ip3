<?php

namespace App\Http\Controllers;

use App\User;
use App\User_notifications;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;

class BaseController extends Controller
{
     public function index() {
             if(Auth::check()){
                 $id = Auth::user()->id;
                 $user_not = User_notifications::count();
                 $forms = DB::table('user_forms')
                     ->leftJoin('forms', 'user_forms.form_id', '=', 'forms.id')
                     ->Where('user_id', $id)
                     ->count();
                 $users = User::count();
                 return view('welcome', compact('user_not','forms', 'users'));
             } else {
                 return view('auth/login');
             }
     }
}
