<?php

namespace App\Http\Controllers;

use App\Groups;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $groups = Groups::all();

        return view('groups.index',compact('groups'));
    }
    public function create()
    {
        return view('groups.create');
    }
    public function created(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:70|min:3|unique:groups',
            'desc' => 'max:200',
        ]);
        $input = Input::get();
       Groups::create([
            'name' => $input['name'],
            'desc' => $input['desc'],
        ]);
        return redirect('users/groups');
    }
    public function delete()
    {
        $input = Input::get('id');
        $user = Groups::find($input);
        $user->delete();
        return redirect('users/groups');
    }
}
