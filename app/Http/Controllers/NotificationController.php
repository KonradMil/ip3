<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Input;
use App\User_notifications;
use App\Notifications;
use App\Groups;
use Illuminate\Http\Request;
use App\Http\Requests;
use App;
use Auth;
use Illuminate\Support\Facades\Redirect;

class NotificationController extends Controller
{

    public function save () {

    }
    public static function sendTo ($id) {
        $name = 'URGENT';
        $content = 'URGENT FORM HAS BEEN SUBMITTED';
        $not = Notifications::create([
            'name' => $name,
            'content' => $content,
            'sender_id' => '999',
            'recipient_id' => $id,
        ]);
        $not_id = $not->id;

        $y = User_notifications::create([
            'user_id' => $id,
            'notification_id' => $not_id,
            'read' => false
        ]);
        $data = [$name, $content];
        $y = pusher()->trigger($id, 'note', $data);

    }
    public function delete(){
        $input = Input::get();
        $m = DB::table('user_notifications')->select('notification_id')->where('id', $input)->get();
        DB::table('user_notifications')->where('id', $input)->delete();
        DB::table('notifications')->where('id', $m)->delete();
        return Redirect::to('notification/showAll')->with('success', true)->with('message','Notification has been deleted!');
    }
    public function showOne() {
        $ar = [];
        if (Auth::check()) {
            $id = Auth::user()->id;

            $not_id = DB::table('user_notifications')->Where('user_id', $id)->select('notification_id')->get();
            foreach($not_id as $n){
                $nots = DB::table('notifications')->Where('id', $n)->select()->get();
                 array_push($ar,$nots[0]);
            }
            return $ar;
        } else {

        }
    }
    public function showAll() {
        $user = DB::table('user_notifications')
            ->leftJoin('notifications', 'user_notifications.notification_id', '=', 'notifications.id')
            ->select('user_notifications.id','user_notifications.user_id', 'notifications.name','notifications.content','notifications.sender_id', 'notifications.created_at')
            ->get();
        return view('notifications.showall', compact('user'));
    }
    public function index () {
        $input = Input::get();
        if($input['notification'] =='gib') {
            $id = Auth::user()->id;
            $user_not = User_notifications::where('id', $id)->get();
            return $user_not;
        } else {
            var_dump('no gib');
        }
    }
    public function sendToGroup () {
        $input = Input::get();
        $id = Auth::user()->id;
        $not = Notifications::create([
            'name' => $input['name'],
            'content' => $input['content'],
            'sender_id' => $id,
            'recipient_id' => $input['to'],
        ]);
        $not_id = $not->id;

        $y = User_notifications::create([
            'user_id' => $id,
            'notification_id' => $not_id,
            'read' => false
        ]);
        $data = [$input['name'], $input['content']];
        $y = pusher()->trigger($input['to'], 'note', $data);
        return Redirect::to('notification/showAll')->with('success', true)->with('message', 'Notification has been sent!');
    }
    public function write(){
        $groups = Groups::get();
        return view('notifications.write', compact('groups'));
    }
    public function sendToAll($msg) {
        $id = Auth::user()->id;
        $not = Notifications::create([
            'name' => $msg['name'],
            'content' => $msg['content'],
            'sender_id' => $id,
            'recipient_id' => 'all',
        ]);
        $not_id = $not->id;

        $y = User_notifications::create([
            'user_id' => $id,
            'notification_id' => $not_id,
            'read' => false
        ]);
        $data = [$msg['name'], $msg['content']];
        $y = pusher()->trigger('all', 'note', $data);
        return true;
    }
}
