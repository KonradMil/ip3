<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User_forms;
use DB;
use App\Forms;
use App\Submittions;
use Illuminate\Http\Request;
use Redis;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\MsgController;
use App\Http\Controllers\NotificationController;

class SubmittionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $forms = Forms::get();
        return view('forms.submit.index', compact('forms'));
    }

    public function showArchived()
    {
        $forms = DB::table('user_forms')
            ->leftJoin('forms', 'user_forms.form_id', '=', 'forms.id')
            ->Where('archived', 1)
            ->select('user_forms.id', 'forms.name', 'user_forms.created_at', 'user_forms.updated_at')
            ->get();
        return view('forms.submit.archived', compact('forms'));
    }

    public function select()
    {
        $id = Auth::user()->id;
        $forms = User_forms::where('user_id', $id)->get();
        $forms = DB::table('user_forms')
            ->leftJoin('forms', 'user_forms.form_id', '=', 'forms.id')
            ->select('user_forms.id', 'forms.name', 'user_forms.created_at', 'user_forms.updated_at', 'user_forms.archived', 'user_forms.user_id', 'highlight')
            ->get();
        return view('forms.submit.select', compact('forms'));
    }

    public function log()
    {
        $input = Input::get();
        $form_id = Forms::where('name', $input["title"])->get();
        $f = $form_id[0];

        $sms_sent = DB::table('msgs')->where('sender_id', $input['id'])->get();
        if(isset($sms_sent[0])) {
            $us = $sms_sent[0];
            //\DB::connection()->enableQueryLog();
            $sms_rec = DB::table('msgs')->where('recipient_id', '447440773005')->where('sender_id', $us['recipient_id'])->get();
            return view('msgs/log', compact('sms_sent', 'sms_rec'));
        }


        //$query = \DB::getQueryLog();
        return Redirect::to('forms/submit/submitted')->with('success', true)->with('message', '0 SMSs!');


    }

    public function display()
    {
        $input = Input::get('id');
        $id = Auth::user()->id;
        $form = DB::table('user_forms')
            ->leftJoin('forms', 'user_forms.form_id', '=', 'forms.id')
            ->Where('user_forms.id', $input)
            ->select('user_forms.id', 'forms.name', 'forms.content', 'submittion_id', 'user_forms.updated_at', 'user_forms.user_id')
            ->get();
        $f = $form[0];

        $hiddenDivBeginning = 'jQuery(document).ready(function($){';

        $values = Redis::HGETALL($f['submittion_id']);


        $counter = 0;
        foreach(array_keys($values) as $keys){

            $hiddenDivBeginning.='$("[name*=\''. $keys .'\']").val(' . $values[$keys] .');';
            $counter++;

        }
        $hiddenDivBeginningEnd = '});';
        $hiddenDivBeginning .= $hiddenDivBeginningEnd;
        return view('forms.submit.display', compact('hiddenDivBeginning', 'form'));
    }

    public function fillin()
    {
        $id = Input::get('id');
        $form = DB::table('forms')->Where('id', $id)->select('id', 'name', 'content')->get();
        $f = $form[0];
        $contents = $f["content"] . '  <div class="col-md-10">


                    <div class="form-group  col-sm-6">
                        <label for="form-high">Highlight
                            <input type="checkbox" class="form-control input-lg " id="form-high" tabindex="1"
                                   name="highlight" data-bind="value: form-highlight"/></label>
                    </div>


        </div>';
        return view('forms.submit.show', compact('form', 'contents'));
    }

    public function save()
    {
        $input = Input::get();

        $id = Auth::user()->id;
        DB::enableQueryLog();
        $max = DB::table('user_forms')->max('submittion_id');
        $query = DB::getQueryLog();
        $lastQuery = end($query);

        $highSubId = (int)$max + 1;
if ($input['highlight'] == 'on') {
    $h = 1;
    NotificationController::sendTo('1');
    MsgController::sendSysSMS('1');
} else {
    $h = 0;
}
        $form = new User_forms();
        $form->user_id = $id;
        $form->form_id = $input['id'];
        $form->submittion_id = $highSubId;
        $form->highlight = $h;
        $form->save();
        foreach(array_keys($input) as $keys){
            $values = Redis::HMSET($highSubId, $keys, json_encode($input[$keys]));
        }

        return Redirect::to('forms/submit/')->with('success', true)->with('message','Report has been submitted!');
    }

    public function archive()
    {
        $input = Input::get();
        DB::table('user_forms')->Where('id', $input)->update(['archived' => 1]);
        return Redirect::to('forms/submit/')->with('success', true)->with('message', 'Form has been archived!');
    }

    public function unarchive()
    {
        $input = Input::get();
        DB::table('user_forms')->Where('id', $input)->update(['archived' => 0]);
        return Redirect::to('forms/submit/')->with('success', true)->with('message', 'Form has been pulled out from archives!');
    }

    public function test()
    {
        $values = Redis::HGETALL('8');
        var_dump($values);
    }
}
