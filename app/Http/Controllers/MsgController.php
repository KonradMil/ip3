<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\Input;
use App\Msgs;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
class MsgController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $msgs = Msgs::all();

        return view('msgs.index',compact('msgs'));
    }
    public function saveToDB() {
        $authentication = new \Esendex\Authentication\LoginAuthentication(
            "EX0207004", // Your Esendex Account Reference
            "konrad@kmilewski.eu", // Your login email address
            "ALpt86s6Nk76" // Your password
        );
        $service = new \Esendex\InboxService($authentication);
        $services = new \Esendex\MessageBodyService($authentication);
        $result = $service->latest();
        $total = $result->totalCount();
        $fetched = $result->count();
        $ids = Auth::user()->id;
        foreach ($result as $message) {
            $body = explode("@@",$services->getMessageBody($message));
            $sub_id = $body[0];
            $msg_body = $body[1];
            $id = $message->id();
            $originator = User::where('id', $message->originator())->get();
            $recipient = User::where('id', $message->recipient())->get();
            Msgs::create([
                'content' => $msg_body,
                'recipient_id' => $message->originator(),
                'sender_id' => $ids,
                'form_id' => $sub_id,
                'msg_id' => $id
            ]);
            $service->deleteInboxMessage($id);
        }

    }
    public function showFormMsgs()
    {
        $input = Input::get('id');
        $msgs = User::find($input);

        return view('msgs.show',compact('msgs'));
    }
    public function showOne()
    {
        $input = Input::get('id');
        $msgs = User::findOrFail($input);

        return view('msgs.show',compact('msgs'));
    }
  public function write () {
      $input = Input::get();
      $sub_id = $input['id'];
        $user_id = $input['us_id'];
      return view('msgs.write', compact('sub_id', 'user_id' ));
  }
    public function sendSms () {
        $input = Input::get();
        $to = $input['to'];
        $from = $input['from'];
        $content = $input['content'];
        $users = User::where('id', $to)->get();
        $user = $users[0];

        $form_name = "FORM@@" .$from . " ";
        $message = new \Esendex\Model\DispatchMessage(
            "07800005476", // Send from
            $user['phone_nr'], // Send to any valid number
            $form_name . $content,
            \Esendex\Model\Message::SmsType
        );
        $authentication = new \Esendex\Authentication\LoginAuthentication(
            "EX0207004", // Your Esendex Account Reference
            "konrad@kmilewski.eu", // Your login email address
            "ALpt86s6Nk76" // Your password
        );
        $service = new \Esendex\DispatchService($authentication);
        $result = $service->send($message);
        $res_id = $result->id();
        Msgs::create([
            'content' => $content,
            'recipient_id' => $to,
            'sender_id' => $from,
            'form_id' => $input['sub_id'],
            'msg_id' => $res_id
        ]);
        $msg = "Msg succesfuly send";
        return view('confirmation',compact('msg'));
    }
    public static function sendSysSms ($to) {

        $from = 'System';
        $content = 'URGENT FORM HAS BEEN SUBMITTED';
        $users = User::where('id', $to)->get();
        $user = $users[0];

        $message = new \Esendex\Model\DispatchMessage(
            $from, // Send from
            $user['phone_nr'], // Send to any valid number
            $content,
            \Esendex\Model\Message::SmsType
        );
        $authentication = new \Esendex\Authentication\LoginAuthentication(
            "EX0207004", // Your Esendex Account Reference
            "konrad@kmilewski.eu", // Your login email address
            "ALpt86s6Nk76" // Your password
        );
        $service = new \Esendex\DispatchService($authentication);
        $result = $service->send($message);
    }
    public function showAll() {
        $client = new Client;
        $auth = base64_encode('konrad@kmilewski:ALpt86s6Nk76');
        $res = $client->get('https://api.esendex.com', ['Authorization' =>  $auth]);
        echo $res->getStatusCode(); // 200
        echo $res->getBody();
    }
    public function receive()
    {
        $input = Input::get('id');
        $msgs = User::findOrFail($input);

        return view('msgs.show',compact('msgs'));
    }
}
