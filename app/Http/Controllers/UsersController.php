<?php

namespace App\Http\Controllers;
use App\Groups;
use DB;
use App\User_group;
use Auth;
use App\Group;
use App\User;
use App\User_groups;
use App\User_roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
class UsersController extends Controller
{
    public function __construct()
{
    $this->middleware('auth');
}
    public function index()
    {
        $users = User::all();

        return view('users.index',compact('users'));
    }
    public function show()
    {
        $input = Input::get('id');
        $user = User::findOrFail($input);

        return view('users.show',compact('user'));
    }
    public function edit()
    {
        $input = Input::get('id');
        $user = User::findOrFail($input);
        $id = Auth::user()->id;
        $groups = Groups::get();
        $user_groups = DB::table('user_groups')
            ->leftJoin('groups', 'user_groups.group_id', '=', 'groups.id')
            ->Where('user_id', $id)
            ->select('groups.id','groups.name')
            ->get();

        return view('users.edit',compact('user','groups','user_groups'));
    }
    public function getUserGroups(){
        $id = Auth::user()->id;
        $user_groups = DB::table('user_groups')
            ->leftJoin('groups', 'user_groups.group_id', '=', 'groups.id')
            ->Where('user_id', $id)
            ->select('groups.id')
            ->get();
        return $user_groups;
    }
    public function edited()
    {

        $input = Input::get();
        var_dump($input);
        $groups = $input['group'];
        foreach($groups as $group) {
            $gr = new User_group();
            $gr->group_id = $group;
            $gr->user_id = Auth::user()->id;
            $gr->save();
        }
        $user = User::find($input['id']);
        $user->name = $input['name'];
        $user->email = $input['email'];
        if( $input['phone'] != null) {
            $user->phone_nr = $input['phone'];
        }
        $user->roles = $input['role'];
        if( $input['password'] != null) {
            $user->password = bcrypt($input['password']);
        }

        $user->save();
        return Redirect::to('users')->with('success', true)->with('message', 'User has been edited!');

    }
    public function create()
    {
        return view('users.create');
    }
    public function whatRole()
    {
        if(Auth::check()) {
            $id = Auth::user()->id;
            $role = DB::table('users')->where('id', $id)->select('roles')->get();
            $r = $role[0];
            return $r['roles'];
        }

    }
    public function created(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:70|min:3',
            'email' => 'required|max:100|email|unique:users',
            'password' => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3'
        ]);
        $input = Input::get();
        User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'roles' => 'user',
            'password' => bcrypt($input['password']),
        ]);
        return redirect('users');
    }
    public function delete()
    {
        $input = Input::get('id');
        $user = User::find($input);
        $user->delete();
        return redirect('users');
    }
}
