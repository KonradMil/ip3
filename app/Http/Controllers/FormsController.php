<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use App\Forms;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
class FormsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
        public function index()
    {
        DB::connection()->enableQueryLog();
        $forms = DB::table('forms')->leftJoin('users', 'forms.created_by', '=', 'users.id')->select('forms.id','forms.name', 'forms.content', 'forms.created_at', 'users.name as nm')->get();

        //$query = DB::getQueryLog();
        //$lastQuery = end($query);
        //var_dump($query);


        return view('forms.index', compact('forms'));
    }
    public function show()
    {

        $input = Input::get('id');
        $form = Forms::findOrFail($input);

        return view('forms.show',compact('form'));
    }
    public function saved()
    {
        $input = Input::get();
        $inp = $input['res'];
        $userId = Auth::id();
        Forms::create([
            'name' => $inp[0],
            'content' => $inp[1],
            'created_by' => $userId,
            'created_at' => Carbon::now()
        ]);
        return view('forms/done')->with('success', true)->with('message','Report has been submitted!');
    }
    public function create()
    {

        return view('forms.create');
    }

    public function delete()
    {
        $input = Input::get('id');
        $form = Forms::find($input);
        $form->delete();
        return redirect('forms');
    }
}
