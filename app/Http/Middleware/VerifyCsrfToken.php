<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'forms/saved',
        'msgs/sendsms',
        'forms/submit/submitnew',
        'api-eu.pusher.com',
        'notification/sendtogroup',
        '/notification/index',
        'users/groups/get'
    ];
}
