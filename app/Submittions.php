<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submittions extends Model
{
    protected $fillable = [
        'user_id', 'submittion_id'
    ];
}
