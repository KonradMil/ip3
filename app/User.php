<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'roles', 'group', 'phone_nr'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function isAdmin() {
        $id = Auth::user()->id;
        $user_role = User::where('id', $id)->get('roles');
        if ($user_role == 'admin') {
            return true;
        } else {
            return false;
        }

    }
}

