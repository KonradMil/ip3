<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_notifications extends Model
{
    protected $fillable = [
        'notification_id', 'user_id', 'read'
    ];
}
