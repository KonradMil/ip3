<?php

namespace App\Providers;

use App\Http\Controllers\UsersController;
use Illuminate\Support\ServiceProvider;

class RolesProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Http\Controllers\UsersController', function(){

            return new UsersController();

        });
    }
    public function provides()
    {
        return ['App\Http\Controllers\UsersController@whatRole'];
    }
}
