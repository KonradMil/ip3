<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function($table)
        {
            $table->increments('id')->unique();
            $table->string('name');
            $table->string('created_by');
            $table->mediumText('content');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('forms');
    }
}
